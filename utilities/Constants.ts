module ThatLevelAgain {
    //UI
    export const BUTTON_KEY: string = 'Button';
    export const TICK_MARK: string = 'Tick';
    export const LEFT_ARROW: string = 'LeftArrow';
    export const SQUARE: string = 'Square';

    // States
    export const PRELOAD_STATE_KEY: string = 'Preload';
    export const MAIN_MENU_STATE_KEY: string = 'MainMenu';
    export const OPTIONS_STATE_KEY: string = 'Options';
    export const GAME_STATE_KEY: string = 'Game';
    export const PAUSE_STATE_KEY: string = 'Pause';
    export const GAME_OVER_STATE_KEY: string = 'GameOver';

    // Atlases
    export const RED_ALIEN_KEY: string = 'RedAlien';
    export const BLUE_ALIEN_KEY: string = 'BlueAlien';
    export const BAD_ALIEN_KEY: string = 'BadAlien';
}