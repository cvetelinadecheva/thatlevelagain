module ThatLevelAgain {
    export class InputHandler {
        public step: number;

        private leftArrowKey: Phaser.Key;
        private rightArrowKey: Phaser.Key;

        private game: Phaser.Game;

        private player: Player;

        constructor(game: Phaser.Game, player: Player) {
            this.game = game;
            this.player = player;
            this.step = 5;

            this.initLeftArrowKey();
            this.initRigthArrowKey();
        }

        update() {
            if(this.leftArrowKey.isDown) {
                this.player.x -= this.step;
            } else if(this.rightArrowKey.isDown) {
                this.player.x += this.step;
            }
        }

        private initLeftArrowKey(): void {
            this.leftArrowKey = this.game.input.keyboard.addKey(Phaser.Keyboard.LEFT);
        }

        private initRigthArrowKey(): void {
            this.rightArrowKey = this.game.input.keyboard.addKey(Phaser.Keyboard.RIGHT);
        }



        public destroy() {
        }
    }
}