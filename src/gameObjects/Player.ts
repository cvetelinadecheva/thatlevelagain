module ThatLevelAgain {
    export class Player extends Phaser.Sprite{


        constructor(game: Phaser.Game) {
            super(game, 150, 200, RED_ALIEN_KEY);

            this.game.physics.enable(this, Phaser.Physics.ARCADE);
            this.game.add.existing(this);

            this.body.immovable = true;
            this.scale.set(0.4);
            //this.body.collideWorldBounds = true;

        }

        update() {

        }

        destroy() {

        }
    }
}