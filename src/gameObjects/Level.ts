module ThatLevelAgain {
    export class Level {
        private game: Phaser.Game;
        public staticTiles: Array<Phaser.Sprite>;

        constructor(game: Phaser.Game) {
            this.game = game;

            this.staticTiles = [];
            this.staticTiles.push(new Phaser.Sprite(this.game, 0, 350, "staticTile"));
            this.staticTiles.push(new Phaser.Sprite(this.game, 500, 350, "staticTile"));

            for(let i: number = 0; i < this.staticTiles.length; i++) {
                this.game.physics.enable(this.staticTiles[i], Phaser.Physics.ARCADE);
                this.staticTiles[i].scale.set(0.3);

                this.staticTiles[i].body.immovable = false;
                this.game.add.existing(this.staticTiles[i]);
            }

        }
    }
}