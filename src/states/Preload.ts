/// <reference path = "../../lib/phaser.d.ts"/>

module ThatLevelAgain {
    export class Preload extends Phaser.State {
        private background: Phaser.Image;

        private alien: Phaser.Sprite;
        private badAlien: Phaser.Sprite;

        private title: Phaser.Text;

        preload() {
            this.game.load.atlas(RED_ALIEN_KEY, 'assets/graphics/aliens/red_alien.png',
                'assets/graphics/aliens/red_alien.json');
            this.game.load.atlasJSONArray(BLUE_ALIEN_KEY, 'assets/graphics/aliens/blue_alien.png',
                'assets/graphics/aliens/blue_alien.json');
            this.game.load.atlasJSONArray(BAD_ALIEN_KEY, 'assets/graphics/aliens/predator_alien.png',
                'assets/graphics/aliens/predator_alien.json');

            this.game.load.image("background", "assets/graphics/backgrounds/space_bg_1.jpg");
            this.game.load.image(BUTTON_KEY, "assets/graphics/ui/btn.png");
            this.game.load.image(SQUARE, "assets/graphics/ui/square.png");
            this.game.load.image(LEFT_ARROW, "assets/graphics/ui/left_arrow.png");
            this.game.load.image(TICK_MARK, "assets/graphics/ui/tick.png");
            this.game.load.image("staticTile", "assets/graphics/tiles/Tile (5).png");
            this.game.load.image("dynamicTile", "assets/graphics/tiles/Tile (15).png");

        }

        create() {
            this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
            this.background = this.game.add.sprite(0, 0, "background");
            this.background.width = this.game.width;
            this.background.height = this.game.height;
            this.game.add.tween(this.background)
                .from({alpha: 0}, 2000, Phaser.Easing.Default, true);

            this.title = this.game.add.text(this.game.width * 0.5, -this.game.height * 0.1,
                "THAT LEVEL AGAIN", null);
            this.title.anchor.set(0.5);
            this.title.align = 'center';
            this.title.font = 'Audiowide';
            this.title.fontSize = 100;
            this.title.stroke = '#000000';
            this.title.strokeThickness = 6;
            this.title.fill = '#43d637';

            this.alien = this.game.add.sprite(0, 0, RED_ALIEN_KEY);
            this.alien.position.set(-100, this.game.height - 200);
            this.alien.anchor.set(0.5);
            this.alien.animations.add('idle', [0, 1, 2], 6, true);
            this.alien.animations.add('run', [12, 13, 14, 15, 16, 17], 16, true);
            this.alien.play('idle');

            this.badAlien = this.game.add.sprite(0, 0, BAD_ALIEN_KEY);
            this.badAlien.position.set(-200, this.game.height * 0.74);
            this.badAlien.anchor.set(0.5);
            this.badAlien.animations.add('run', [12, 13, 14, 15, 16, 17], 16, true);
            this.badAlien.play('run');

            // this.game.add.tween(this.alien)
            //     .to({angle: 45, x: 10}, 1000, Phaser.Easing.Default, true)
            //     .onComplete.add(() => {
            //     this.game.add.tween(this.alien)
            //         .to({angle: 0, x: -200}, 1000, Phaser.Easing.Default, true, 500)
            //         .onComplete.add(() => {
            //         this.alien.position.set(this.game.width + 100, 200);
            //         this.game.add.tween(this.alien)
            //             .to({angle: -45, x: this.game.width - 50}, 1000, Phaser.Easing.Default,
            //                 true)
            //             .onComplete.add(() => {
            //             this.game.add.tween(this.alien).to({angle: 0, x: this.game.width + 120}, 1000,
            //                 Phaser.Easing.Default, true, 500)
            //                 .onComplete.add(() => {
            //                 this.alien.position.set(-200, this.game.height * 0.7);
            //                 this.alien.animations.play('run');
            //                 this.game.add.tween(this.alien)
            //                     .to({x: this.game.width + 200}, 3000, Phaser.Easing.Default,
            //                         true);
            //                 this.game.add.tween(this.badAlien)
            //                     .to({x: this.game.width + 200}, 3000, Phaser.Easing.Default,
            //                         true, 1000)
            //                     .onComplete.add(() => {
                                this.game.state.start(GAME_STATE_KEY);
            //                 }, this);
            //                 this.game.add.tween(this.title)
            //                     .to({y: this.game.height * 0.4}, 1000, Phaser.Easing.Bounce.In, true);
            //             }, this);
            //         }, this);
            //     }, this);
            // }, this);
        }

        shutdown() {
            this.background.destroy();
            this.background = null;

            this.alien.destroy();
            this.alien = null;
            this.badAlien.destroy();
            this.badAlien = null;

            this.title.destroy();
            this.title = null;
        }
    }
}