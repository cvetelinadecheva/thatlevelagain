/// <reference path = "../../lib/phaser.d.ts"/>

module ThatLevelAgain{
    export class Game extends Phaser.State{
        private player: Player;
        private level: Level;
        private inputHnadler: InputHandler;

        create(){
            this.game.physics.startSystem(Phaser.Physics.ARCADE);

            this.level = new Level(this.game);
            this.player = new Player(this.game);
            this.inputHnadler = new InputHandler(this.game, this.player);


        }

        update(){
            this.inputHnadler.update();

            this.game.physics.arcade.collide(this.level.staticTiles[0], this.player.body);
            this.game.physics.arcade.collide(this.level.staticTiles[1], this.player.body);
        }

        render(){
            this.game.debug.body(this.level.staticTiles[0]);
            this.game.debug.body(this.level.staticTiles[1]);
            this.game.debug.body(this.player);
        }
    }
}