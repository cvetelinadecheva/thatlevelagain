module ThatLevelAgain{
    export class GameOver extends Phaser.State{
        // ADD CLASS PROPERTIES HERE

        create(){
            // INITIALIZE GAME ELEMENTS HERE
        }

        update(){
            // THIS METHOD IS CALLED 60 TIMES A SECOND. PUT YOUR GAME LOGIC HERE
        }

        render(){
            // USED FOR DEBUGGING, e.g

        }
    }
}