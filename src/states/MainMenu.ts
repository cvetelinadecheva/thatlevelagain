module ThatLevelAgain {
    export class MainMenu extends Phaser.State {
        private background: Phaser.Image;

        private startButton: Phaser.Button;
        private optionButton: Phaser.Button;
        private exitButton: Phaser.Button;

        private title: Phaser.Text;
        private startText: Phaser.Text;
        private optionText: Phaser.Text;
        private exitText: Phaser.Text;

        create() {
            this.background = this.game.add.image(0, 0, "background");
            this.background.width = this.game.width;
            this.background.height = this.game.height;

            this.title = this.game.add.text(this.game.width * 0.5, this.game.height * 0.4,
                "THAT LEVEL AGAIN", null);
            this.title.anchor.set(0.5);
            this.title.align = 'center';
            this.title.font = 'Audiowide';
            this.title.fontSize = 100;
            this.title.stroke = '#000000';
            this.title.strokeThickness = 6;
            this.title.fill = '#43d637';

            this.game.add.tween(this.title)
                .to({y: this.game.height * 0.1}, 1000, Phaser.Easing.Default, true)
                .onComplete.add(() => {
                this.game.add.tween(this.startButton)
                    .to({y: this.game.height * 0.4}, 500, Phaser.Easing.Default, true)
                    .onComplete.add(() => {
                    this.game.add.tween(this.optionButton)
                        .to({y: this.game.height * 0.55}, 500, Phaser.Easing.Default, true)
                        .onComplete.add(() => {
                        this.game.add.tween(this.exitButton)
                            .to({y: this.game.height * 0.8}, 500, Phaser.Easing.Default, true);
                    }, this);
                }, this);

            }, this);

            this.initStartButton();
            this.initOptionsButton();
            this.initExitButton();

            this.initMainMenuLabels();

            this.startButton.addChild(this.startText);
            this.optionButton.addChild(this.optionText);
            this.exitButton.addChild(this.exitText);
        }

        private initStartButton(): void {
            this.startButton = this.game.add.button(this.game.world.centerX, -100, BUTTON_KEY,
                () => {
                    this.game.state.start(GAME_STATE_KEY);
                });
            this.startButton.anchor.set(0.5, 1);
            this.startButton.scale.set(0.4);

            this.startButton.onInputOver.add(() => {
                this.startButton.scale.set(0.45);
            }, this);
            this.startButton.onInputOut.add(() => {
                this.startButton.scale.set(0.4);
            }, this);
        }

        private initOptionsButton(): void {
            this.optionButton = this.game.add.button(this.game.world.centerX, -100, BUTTON_KEY,
                () => {
                    this.game.state.start(OPTIONS_STATE_KEY);
                }, this, 1);
            this.optionButton.anchor.set(0.5, 1);
            this.optionButton.scale.set(0.4);

            this.optionButton.onInputOver.add(() => {
                this.optionButton.scale.set(0.45);
            }, this);
            this.optionButton.onInputOut.add(() => {
                this.optionButton.scale.set(0.4);
            }, this);
        }

        private initExitButton(): void {
            this.exitButton = this.game.add.button(this.game.world.centerX, -100, BUTTON_KEY,
                () => {
                    this.game.state.destroy();
                }, this, 1);
            this.exitButton.anchor.set(0.5, 1);
            this.exitButton.scale.set(0.4);

            this.exitButton.onInputOver.add(() => {
                this.exitButton.scale.set(0.45);
            }, this);
            this.exitButton.onInputOut.add(() => {
                this.exitButton.scale.set(0.4);
            }, this);
        }

        private initMainMenuLabels() {
            this.startText = this.game.add.text(0, 0, "START", null);
            this.setStyle(this.startText);

            this.optionText = this.game.add.text(0, 0, "OPTIONS", null);
            this.setStyle(this.optionText);

            this.exitText = this.game.add.text(0, 0, "EXIT", null);
            this.setStyle(this.exitText);
        }

        private setStyle(text: Phaser.Text) {
            text.anchor.set(0.5, 1.3);
            text.align = 'center';
            text.font = 'Audiowide';
            text.fontSize = 30;
            text.stroke = '#000000';
            text.strokeThickness = 6;
            text.fill = '#43d637';
            text.setScaleMinMax(1, 1, 1, 1);
        }

        shutdown() {
            this.background.destroy();
            this.background = null;

            this.startButton.destroy();
            this.startButton = null;
            this.optionButton.destroy();
            this.optionButton = null;
            this.exitButton.destroy();
            this.exitButton = null;

            this.title.destroy();
            this.title = null;
            this.startText.destroy();
            this.startText = null;
            this.optionText.destroy();
            this.optionText = null;
            this.exitText.destroy();
            this.exitText = null;
        }
    }
}