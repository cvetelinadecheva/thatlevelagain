module ThatLevelAgain{
    export class Options extends Phaser.State{
        private background: Phaser.Image;
        private tick: Phaser.Image;
        private tick2: Phaser.Image;
        private tick3: Phaser.Image;
        private musicBox: Phaser.Image;
        private soundsBox: Phaser.Image;
        private fullScreebBox: Phaser.Image;
        private leftArrow: Phaser.Image;

        private title: Phaser.Text;
        private soundsTitle: Phaser.Text;
        private musicTitle: Phaser.Text;
        private fullScreenTitle: Phaser.Text;

        create(){
            this.background = this.game.add.image(0, 0, "background");
            this.background.width = this.game.width;
            this.background.height = this.game.height;

            this.leftArrow = this.game.add.button(10, this.game.height - 110, LEFT_ARROW,
                () => {
                    this.game.state.start(MAIN_MENU_STATE_KEY);
                }, this, 1);
            this.leftArrow.scale.set(0.4);

            this.title = this.game.add.text(this.game.width * 0.5, this.game.height * 0.2,
                "THAT LEVEL AGAIN\nOPTIONS", null);
            this.setStyle(this.title);
            this.title.anchor.set(0.5);
            this.title.fontSize = 100;

            this.musicTitle = this.game.add.text(this.game.width * 0.2, this.game.height * 0.45,
                "Music:", null);
            this.setStyle(this.musicTitle);
            this.musicBox = this.game.add.button(this.game.width * 0.7, this.game.height * 0.43, SQUARE,
                () => {
                if(this.tick.alpha == 1) {
                    this.tick.alpha = 0;
                } else {
                    this.tick.alpha = 1;
                }
                }, this, 1);
            this.musicBox.scale.set(0.2);
            this.tick = this.game.add.image(this.game.width * 0.7, this.game.height * 0.43, TICK_MARK);
            this.tick.scale.set(0.5);

            this.soundsTitle = this.game.add.text(this.game.width * 0.2, this.game.height * 0.6,
                "Sound: ", null);
            this.setStyle(this.soundsTitle);
            this.soundsBox = this.game.add.button(this.game.width * 0.7, this.game.height * 0.58, SQUARE,
                () => {
                    if(this.tick2.alpha == 1) {
                        this.tick2.alpha = 0;
                    } else {
                        this.tick2.alpha = 1;
                    }
                }, this, 1);
            this.soundsBox.scale.set(0.2);
            this.tick2 = this.game.add.image(this.game.width * 0.7, this.game.height * 0.58, TICK_MARK);
            this.tick2.scale.set(0.5);

            this.fullScreenTitle = this.game.add.text(this.game.width * 0.2, this.game.height * 0.75,
                "Full screen: ", null);
            this.setStyle(this.fullScreenTitle);
            this.fullScreebBox = this.game.add.button(this.game.width * 0.7, this.game.height * 0.73, SQUARE,
                () => {
                    if(this.tick3.alpha == 1) {
                        this.tick3.alpha = 0;
                        this.game.scale.stopFullScreen();
                    } else {
                        this.tick3.alpha = 1;
                        this.game.scale.startFullScreen(false);
                    }
                }, this, 1);
            this.fullScreebBox.scale.set(0.2);
            this.tick3 = this.game.add.image(this.game.width * 0.7, this.game.height * 0.73, TICK_MARK);
            this.tick3.scale.set(0.5);
            this.tick3.alpha = 0;


            this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
           // this.game.input.onDown.add(this.goFullScreen, this);
        }

        update(){

        }

        private setStyle(text: Phaser.Text) {
          //  text.anchor.set(0.5);
            text.align = 'center';
            text.font = 'Audiowide';
            text.fontSize = 60;
            text.stroke = '#000000';
            text.strokeThickness = 6;
            text.fill = '#43d637';
        }
    }
}