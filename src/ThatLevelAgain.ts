/// <reference path = "../lib/phaser.d.ts"/>

module ThatLevelAgain {
    class ThatLevelAgain extends Phaser.Game {
        constructor(width?:number, height?:number) {
            super(width, height, Phaser.AUTO, 'phaser-div');

            this.state.add(PRELOAD_STATE_KEY, Preload, true);
            this.state.add(MAIN_MENU_STATE_KEY, MainMenu, false);
            this.state.add(OPTIONS_STATE_KEY, Options, false);
            this.state.add(GAME_STATE_KEY, Game, false);
            this.state.add(PAUSE_STATE_KEY, Pause, false);
            this.state.add(GAME_OVER_STATE_KEY, GameOver, false);
        }
    }

    window.onload = ()=> {
        new ThatLevelAgain(1280, 720);
    }
}